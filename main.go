package main

import (
	"bufio"
	"fmt"
	"math"
	"os"
	"sort"
	"strconv"
	"strings"
)

func meanValue(x []float64) float64 {
	var result float64
	for i := range x {
		result += x[i]
	}
	return result / float64(len(x))
}

func variance(x []float64) float64 {
	var result float64
	mean := meanValue(x)
	for _, val := range x {
		result += (val - mean) * (val - mean)
	}
	return result / float64(len(x))
}

func outliers(x []float64, limit float64) []float64 {
	var result []float64
	deviation := math.Sqrt(variance(x))
	mean := meanValue(x)
	anomaly := deviation * limit
	lowerLimit := mean - anomaly
	upperLimit := mean + anomaly
	fmt.Println("limits:", lowerLimit, upperLimit)

	for _, val := range x {
		if val < lowerLimit || val > upperLimit {
			result = append(result, val)
		}
	}

	return result

}

func main() {
	if len(os.Args) != 3 {
		fmt.Printf("Usage: %s filename limit\n", os.Args[0])
		return
	}

	filename := os.Args[1]
	file, err := os.Open(filename)
	if err != nil {
		panic(err)
	}
	defer file.Close()

	limit, err := strconv.ParseFloat(os.Args[2], 64)
	if err != nil {
		panic(err)
	}

	reader := bufio.NewReader(file)
	var (
		line string
		data []float64
	)
	for ; err == nil; line, err = reader.ReadString('\n') {
		if line == "" {
			continue
		}

		line = strings.TrimSpace(line)
		value, err := strconv.ParseFloat(line, 64)
		if err == nil {
			data = append(data, value)
		}
	}

	sort.Float64s(data)
	out := outliers(data, limit)
	fmt.Println(out)
}
